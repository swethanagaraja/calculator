import static org.junit.Assert.*;

import org.junit.Test;

public class AppTest 
{
	@Test
	public void testApp()
    {
        assertEquals(11,new App().add(3,8));
        assertEquals(5,new App().sub(10,5));
    }
}
